import * as PIXI from 'pixi.js';
import * as Particles from '@pixi/particle-emitter';
import { Box } from "./codec";
import { PartMeta } from "./parts";

let _white_box;
{
	const el_canvas = document.createElement("canvas");
	el_canvas.width = 1; el_canvas.height = 1;
	const el_context = el_canvas.getContext("2d");
	el_context.fillStyle = "white";
	el_context.fillRect(0,0,1,1);
	_white_box = PIXI.Texture.from(el_canvas);
	_white_box.defaultAnchor.set(0.5,0.5);
}
export const white_box: PIXI.Texture = _white_box;

export interface ParticleManager {
	update_particles(delta_seconds: number): boolean;
}

type Particle7573 = Particles.Particle & { vel: PIXI.Point };
class ThrusterParticleBehavior implements Particles.behaviors.IEmitterBehavior {
	voffset = new PIXI.Point();
	vmatrix = new PIXI.Matrix();
	vvel = new PIXI.Point();

	static new(config: any): Particles.behaviors.IEmitterBehavior { return new ThrusterParticleBehavior(config); }
	constructor(config: any) {
	}

	static readonly type: string = "thruster";
	readonly type = ThrusterParticleBehavior.type;

	readonly order = 100;
	initParticles(particle: Particles.Particle) {
		while (particle != null) {
			const emitter = particle.emitter as ThrusterEmitter;
			this.voffset.copyFrom(emitter.offset);
			this.vmatrix.set(1, 0, 0, 1, 0, 0).rotate(emitter.part.sprite.rotation);
			this.vmatrix.apply(this.voffset, this.voffset);
			particle.x += this.voffset.x;
			particle.y += this.voffset.y;
			this.vvel.copyFrom(emitter.vel);
			const out = new PIXI.Point();
			this.vmatrix.apply(this.vvel, out);
			out.x += emitter.part.particle_speed_x;
			out.y += emitter.part.particle_speed_y;
			(particle as Particle7573).vel = out;
			particle = particle.next;
		}
	}
	
	updateParticle(particle: Particles.Particle, deltaSecs: number) {
		particle.position.x += (particle as Particle7573).vel.x * deltaSecs;
		particle.position.y += (particle as Particle7573).vel.y * deltaSecs;
	}
}
Particles.Emitter.registerBehavior(ThrusterParticleBehavior);

export interface ParticleConfig7573 extends Particles.EmitterConfigV3 {
	behaviors: any;
}

export const BeamoutParticleConfig: ParticleConfig7573 = {
	lifetime: { min: 1, max: 2 },
	frequency: 0.001,
	pos: { x: 0, y: 0 },
	maxParticles: 10,
	autoUpdate: false,
	emitterLifetime: 0.5,
	behaviors: [
		{
			type: "rotationStatic",
			config: { min: 0, max: 360 },
		},
		{
			type: "scale",
			config: { scale: { list: [
				{ time: 0, value: 0.2 },
				{ time: 1, value: 0.5 }
			] } }
		},
		{
			type: "moveSpeed",
			config: {
				speed: {
					list: [ { time: 0, value: 5}, { time: 1, value: 2.5 } ]
				}
			}
		},
		{
			type: "color",
			config: { color: { list: [
				{ time: 0, value: "#ffffff" },
				{ time: 1, value: "#5fcc4b" },
			] } }
		},
		{
			type: "textureSingle",
			config: {
				texture: white_box
			}
		},
	],
};

export const IncinerationParticleConfig: ParticleConfig7573 = {
	lifetime: { min: 1, max: 2 },
	frequency: 0.001,
	pos: { x: 0, y: 0 },
	maxParticles: 10,
	autoUpdate: false,
	emitterLifetime: 0.5,
	behaviors: [
		{
			type: "rotationStatic",
			config: { min: 0, max: 360 },
		},
		{
			type: "scale",
			config: { scale: { list: [
				{ time: 0, value: 0.2 },
				{ time: 1, value: 0.5 }
			] } }
		},
		{
			type: "moveSpeed",
			config: {
				speed: {
					list: [ { time: 0, value: 5}, { time: 1, value: 2.5 } ]
				}
			}
		},
		{
			type: "color",
			config: { color: { list: [
				{ time: 0.0, value: "#D73502" },
				{ time: 0.2, value: "#FAC000" },
				{ time: 1.0, value: "#333333" }
			] }	},
		},
		{ type: "textureSingle", config: { texture: white_box } },
	],
};

export const ThrusterParticleConfig: ParticleConfig7573 = {
	lifetime: { min: 0.5, max: 2 },
	frequency: 0.05,
	pos: { x: 0, y: 0 },
	autoUpdate: false,
	emit: false,

	behaviors: [
		{
			type: "scale",
			config: { scale: { list: [
				{ time: 0, value: 0.2 },
				{ time: 1, value: 1   },
			] } }
		},
		{
			type: "thruster",
			config: {

			}
		},
		{
			type: "color",
			config: { color: { list: [
				{ time: 0.0, value: "#D73502" },
				{ time: 0.2, value: "#FAC000" },
				{ time: 1.0, value: "#333333" }
			] } }
		},
		{ type: "textureSingle", config: { texture: white_box } },
	],

    //speed: { list: [ { time: 0, value: 1.5 } ] },
	//startRotation: { min: -8, max: 8 },
	//  acceleration: { x: 0.5, y: 0.5 },
};

export const CoreParticleConfig: ParticleConfig7573 = {
	lifetime: { min: 0.5, max: 2 },
	frequency: 0.05,
	pos: { x: 0, y: 0 },
	autoUpdate: false,
	emit: false,

	behaviors: [
		{
			type: "scale",
			config: { scale: { list: [
				{ time: 0, value: 0.1 },
				{ time: 1, value: 0.5 },
			] } }
		},
		{
			type: "thruster",
			config: {

			}
		},
		{
			type: "color",
			config: { color: { list: [
				{ time: 0.0, value: "#9CDEEB" },
				{ time: 0.2, value: "#66BEF9" },
				{ time: 1.0, value: "#043F98" },
			] } }
		},
		{ type: "textureSingle", config: { texture: white_box } },
	],

    //speed: { list: [ { time: 0, value: 1.5 } ] },
	//startRotation: { min: -8, max: 8 },
	//  acceleration: { x: 0.5, y: 0.5 },
};

export const SuperThrusterParticleConfig: ParticleConfig7573 = {
	lifetime: { min: 0.5, max: 2 },
	frequency: 0.05,
	pos: { x: 0, y: 0 },
	autoUpdate: false,
	emit: false,

	behaviors: [
		{
			type: "scale",
			config: { scale: { list: [
				{ time: 0, value: 0.2 },
				{ time: 1, value: 1   },
			] } }
		},
		{
			type: "thruster",
			config: {

			}
		},
		{
			type: "color",
			config: { color: { list: [
				{ time: 0.0, value: "#d10707" },
				{ time: 0.2, value: "#D73502" },
				{ time: 1.0, value: "#333333" }
			] } }
		},
		{ type: "textureSingle", config: { texture: white_box } },
	],
};

export const EcoThrusterParticleConfig: ParticleConfig7573 = {
	lifetime: { min: 0.5, max: 2 },
	frequency: 0.05,
	pos: { x: 0, y: 0 },
	autoUpdate: false,
	emit: false,

	behaviors: [
		{
			type: "scale",
			config: { scale: { list: [
				{ time: 0, value: 0.2 },
				{ time: 1, value: 0.7 },
			] } }
		},
		{
			type: "thruster",
			config: {

			}
		},
		{
			type: "color",
			config: { color: { list: [
				{ time: 0.0, value: "#ffff79" },
				{ time: 0.8, value: "#b1b1b1" },
				{ time: 1.0, value: "#474747" },
			] } }
		},
		{ type: "textureSingle", config: { texture: white_box } },
	],
};

export class ThrusterEmitter extends Particles.Emitter {
	offset: PIXI.Point;
	vel: PIXI.Point;
	part: PartMeta;

	constructor(part: PartMeta, offset: PIXI.Point, vel: PIXI.Point, parent: PIXI.Container, config: Particles.EmitterConfigV3) {
		super(parent, config);
		this.part = part;
		this.offset = offset;
		this.vel = vel;
	}
}
