import { global } from "./index";
import { ToServerMsg } from "./codec";

let message_button = (document.querySelector("#chat_send") as HTMLInputElement);
let message_box = (document.querySelector("#chat_input") as HTMLInputElement);
let message_root = (document.querySelector("#chat_messages") as HTMLDivElement);
const root_root = document.querySelector("#chat_root_root") as HTMLDivElement;
const chat_toggle = document.querySelector("#chat_toggle") as HTMLButtonElement;
const chat_root = document.querySelector("#chat_root") as HTMLDivElement;
const chat_emoji_button = document.querySelector("#chat_emojies") as HTMLDivElement;
const emote_select_div = document.querySelector("#emote_select") as HTMLDivElement;

var emojies: String[][] = [
    ["😀", "smile", "smiley", "grin"],
    ["😂", "joy", "laugh", "laughing", "lol"],
    ["🤑", "rich"],
    ["🙄", "eye_roll", "rolling_eyes"],
    ["🤢", "sick", "vomit"],
    ["😢", "cry", "tear"],
    ["😭", "sob"],
    ["😉", "wink"],
    ["😱", "omg"],
    ["😠", "mad", "anger"],  
    ["😡", "rage"],
    ["🤔", "think"],
    ["😏", "smirk"],
    ["😴", "sleep"],
    ["👌", "ok"],
    ["👍", "tu"],
    ["👎", "td"],
    ["👏", "clap"],
    ["👀", "eyes"],
    ["💀", "skull"],
    ["🤖", "robot"],
    ["🌎", "earth", "globe"],
    ["❤️", "heart", "love_heart"],
    ["🔥", "fire", "flame"],
    ["🚀", "rocket", "ship", "space_ship"],
    ["ඞ", "sus"]
]
let emojie_selector_open = false;

for(var i = 0; i < emojies.length; i++) {
    let p = document.createElement("p");
    p.innerText = emojies[i][0].toString();
    emote_select_div.appendChild(p);
    p.addEventListener('click', function() {
        message_box.value = message_box.value + p.innerText;
        message_box.focus();
    })
}

message_box.addEventListener("keydown", key => {
    if(key.keyCode == 13) {
		global.chat.send_message(message_box.value);
        message_box.value = "";
        if(emojie_selector_open) { toggleEmoteArea(); }
		key.stopPropagation();
    }
});

message_button.addEventListener("keydown", e => { e.stopPropagation(); });
message_button.addEventListener("keypress", e => { e.stopPropagation(); });
message_button.addEventListener("keyup", e => { e.stopPropagation(); });

message_button.onclick = function() { 
    global.chat.send_message(message_box.value);
    if(emojie_selector_open) { toggleEmoteArea(); }
}

function toggleEmoteArea() {
    emojie_selector_open = !emojie_selector_open;
    if(!emojie_selector_open) {
        emote_select_div.style.opacity = "0%";
        setTimeout(() => {
            emote_select_div.style.visibility = "hidden";
        }, 100);
        chat_emoji_button.style.color = "white";
    } else {
        emote_select_div.style.visibility = "visible";
        emote_select_div.style.opacity = "100%";
        chat_emoji_button.style.color = "#ffcc4d";
    }
}

chat_emoji_button.addEventListener('click', toggleEmoteArea);

export class Chat {
	constructor() {
	   chat_toggle.addEventListener("click", _ => { this.toggle(); });
	}

    is_open = false;
	notifications: HTMLDivElement[] = [];

	toggle() {
		if (!this.is_open) this.open();
		else this.close();
	}

    open() {
        if(this.is_open) return;
        this.is_open = true;
		chat_toggle.innerText = "Close";
		chat_root.classList.add("open");
		for (const message of this.notifications) message.classList.add("outro");
		this.notifications = [];
		message_box.blur();
        if(emojie_selector_open) { toggleEmoteArea(); }
    }

    close() {
        if(!this.is_open) return;
        this.is_open = false;
		chat_toggle.innerText = "Chat";
		chat_root.classList.remove("open");
        if(emojie_selector_open) { toggleEmoteArea(); }
    }

    receive_message(content: string, username: string, color: string) {
        let temp = (document.querySelector("#message_template") as HTMLTemplateElement);
        let clone = (temp.content.cloneNode(true) as HTMLDivElement);
        let message = (clone.firstElementChild as HTMLParagraphElement);
        content = this.emojify(content)
        message.innerText = `${username}: ${content}`;
        message.style.color = color;
        message_root.appendChild(clone);
        message_root.scrollTop = message_root.offsetTop + message_root.scrollHeight;
        if(!this.is_open) {
            let notification = document.createElement("div") as HTMLDivElement;
			notification.classList.add("chat_notification");
            notification.innerText = `${username}: ${content}`;
            notification.style.color = color;
            root_root.insertBefore(notification, chat_root);
			this.notifications.push(notification);
            if(this.notifications.length > 3) this.notifications.shift().classList.add("outro");
            setTimeout(() => {
				const i = this.notifications.indexOf(notification);
				if (i > -1) this.notifications.splice(i, 1);
				notification.classList.add("outro");
				setTimeout(() => { notification.parentElement.removeChild(notification); }, 1000);
            }, 9500);
        }
    }

    send_message(content: string) {
        content = this.unemojify(content);
        if(content.replace(/\s/g, '').length) {
            if(content.length >= 201) {
                content = content.substring(0, 201);
            }
            global.socket.send(new ToServerMsg.SendChatMessage(content).serialize());
        }
		message_box.blur();
        message_box.value = "";
    }

    emojify(message: string) {
        var emojied: string = message;
        for(var sus = 0; sus < emojied.length; sus++) {
            for(var i = 0; i < emojies.length; i++) {
                for(var x = 1; x < emojies[i].length; x++) {
                    emojied = emojied.replace(`:${emojies[i][x].toLocaleLowerCase()}:`, emojies[i][0].toString());
                }
            }
        }
        return emojied;
    }

    unemojify(message: string) {
        var unemojied: string = message;
        for(var sus = 0; sus < unemojied.length; sus++) {
            for(var i = 0; i < emojies.length; i++) {
                unemojied = unemojied.replace(emojies[i][0].toString(), `:${emojies[i][1]}:`);
            }
        }
        return unemojied;
    }
}

