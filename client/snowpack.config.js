// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
	mount: {
		"src": {
			url: "/",
			static: false,
		},
		"dist": {
			url: "/",
			static: true,
		}
	},
	plugins: [
		/* ... */
	],
	packageOptions: {
		source: "local",
		polyfillNode: true,
		/* ... */
	},
	devOptions: {
		port: parseInt(process.env["SNOWPACK_PORT"] ?? 8080),
		open: "none",
		hmr: false,
	},
	buildOptions: {
		baseUrl: "/game"
		/* ... */
	},
};
